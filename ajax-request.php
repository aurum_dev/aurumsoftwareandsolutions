<?php
    
    //$mysqli = new mysqli("uatdatabase.cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com","uatdbadmin","VisitorAurumuat","aurumproptech");
    $mysqli = new mysqli("localhost","root","","aurum_software");
    // Check connection
    if ($mysqli -> connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
        exit();
    }


    if(isset($_POST['contact']) && $_POST['contact'] == 1) {
        parse_str($_POST['data'], $data);
        $name = trim($data['name']);
        $email = trim($data['email']);
        $mobile = trim($data['mobile']);
        $message = trim($data['message']);

        $stmt = $mysqli->prepare("INSERT INTO db_contact_us (name, email, phone, message) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $name, $email, $mobile, $message);
        $stmt->execute();
        if($mysqli->insert_id > 0) {
            echo json_encode(array("status" => 1));
        }else{
            echo json_encode(array("status" => 0));
        }
        $stmt->close();
        die();
    }

    if(isset($_POST["newsletter"]) && $_POST["newsletter"] ==1) {
        $email = trim($_POST['email']);
        $stmt = $mysqli->prepare("SELECT * FROM db_newsletter WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0) {
            echo json_encode(array("status" => 1, "msg" => "Email already exist."));
        }else{
            $stmt = $mysqli->prepare("INSERT INTO db_newsletter (email) VALUES (?)");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            if($mysqli->insert_id > 0) {
                echo json_encode(array("status" => 1, "msg" => "Email has been submitted successfully."));
            }else{
                echo json_encode(array("status" => 0, "msg" => "Email has not been submitted."));
            }
        }
        $stmt->close();
        die();
    }
?>